package day3

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.locationtech.jts.geom.Coordinate

internal class ManhattanDistanceTest {
    private val examples = listOf(
        Coordinate(0.0, 42.0) to 42.0,
        Coordinate(42.0, 0.0) to 42.0,
        Coordinate(12.0, 34.0) to 46.0,
        Coordinate(-10.0, 23.0) to 33.0,
        Coordinate(16.0, -9.0) to 25.0
    )

    @TestFactory
    fun testDistance() = examples.map { (point: Coordinate, expectedDistance: Double) ->
        dynamicTest("Manhattan distance for point $point is $expectedDistance") {
            assertEquals(expectedDistance, manhattanDistance(point))
        }
    }

    @Test
    fun testRelativeDistance() {
        assertEquals(
            42.0, manhattanDistance(
                Coordinate(100.0, 50.0),
                from = Coordinate(70.0, 38.0)
            )
        )
    }
}
