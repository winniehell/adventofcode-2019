package day3

import getInput
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.locationtech.jts.geom.Coordinate

internal class WireTest {
    private val parsingExamples = listOf(
        "R75" to listOf(
            Coordinate(0.0, 0.0) to Coordinate(75.0, 0.0)
        ),
        "R75,D30,R83" to listOf(
            Coordinate(0.0, 0.0) to Coordinate(75.0, 0.0),
            Coordinate(75.0, 0.0) to Coordinate(75.0, -30.0),
            Coordinate(75.0, -30.0) to Coordinate(158.0, -30.0)
        )
    )

    @TestFactory
    fun testParsing() = parsingExamples.map { (input: String, expectedOutput: List<Pair<Coordinate, Coordinate>>) ->
        dynamicTest("Segments for input $input is $expectedOutput") {
            assertEquals(
                expectedOutput,
                Wire(input).segments.map { (lineString, _) ->
                    Pair(
                        lineString.coordinates[0],
                        lineString.coordinates[1]
                    )
                })
        }
    }

    @Test
    fun testIntersections() {
        val firstWire = Wire("R10,U10")
        val secondWire = Wire("U5,R20")
        val expectedIntersections = listOf(
            Coordinate(10.0, 5.0)
        )
        assertEquals(expectedIntersections, firstWire.intersections(secondWire).map { (point, _, _) -> point })
    }

    private val distanceExamples = listOf(
        Triple(
            Wire("R75,D30,R83,U83,L12,D49,R71,U7,L72"),
            Wire("U62,R66,U55,R34,D71,R55,D58,R83"),
            159.0
        ),
        Triple(
            Wire("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"),
            Wire("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"),
            135.0
        )
    )

    @TestFactory
    fun testMinDistance() = distanceExamples.map { (firstWire: Wire, secondWire: Wire, expectedDistance: Double) ->
        dynamicTest("Distance for wires $firstWire and $secondWire is $expectedDistance") {
            val intersections = firstWire.intersections(secondWire)
            val minDistance = intersections.map { (point, _, _) -> manhattanDistance(point) }.min()
            assertEquals(expectedDistance, minDistance)
        }
    }

    @Test
    fun partOne() {
        val (firstWire, secondWire) = getInput(day = 3).readLines().map { Wire(it.trim()) }
        val intersections = firstWire.intersections(secondWire)
        val minDistance = intersections.map { (point, _, _) -> manhattanDistance(point) }.min()
        assertEquals(1285.0, minDistance)
    }

    private val signalDelayExamples = listOf(
        Triple(
            Wire("R75,D30,R83,U83,L12,D49,R71,U7,L72"),
            Wire("U62,R66,U55,R34,D71,R55,D58,R83"),
            610.0
        ),
        Triple(
            Wire("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51"),
            Wire("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"),
            410.0
        )
    )

    @TestFactory
    fun testMinSignalDelay() =
        signalDelayExamples.map { (firstWire: Wire, secondWire: Wire, expectedSignalDelay: Double) ->
            dynamicTest("Signal delay for wires $firstWire and $secondWire is $expectedSignalDelay") {
                val minSignalDelay = firstWire.findMinimumSignalDelayIntersection(secondWire)
                assertEquals(expectedSignalDelay, minSignalDelay)
            }
        }

    @Test
    fun partTwo() {
        val (firstWire, secondWire) = getInput(day = 3).readLines().map { Wire(it.trim()) }
        val minSignalDelay = firstWire.findMinimumSignalDelayIntersection(secondWire)
        assertEquals(14228.0, minSignalDelay)
    }
}
