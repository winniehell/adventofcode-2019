package day1

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class WithoutFuelMassTest {
    private val examples = listOf(
        12 to 2,
        14 to 2,
        1969 to 654,
        100756 to 33583
    )

    @TestFactory
    fun testFuel() = examples.map { (massInElephantUnits: Int, expectedFuel: Int) ->
        dynamicTest("Fuel for $massInElephantUnits is $expectedFuel") {
            assertEquals(expectedFuel, fuelForModuleMass(massInElephantUnits))
        }
    }
}
