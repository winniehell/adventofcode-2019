package day1

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class WithFuelMassTest {
    private val examples = listOf(
        14 to 2,
        1969 to 966,
        100756 to 50346
    )

    @TestFactory
    fun testFuel() = examples.map { (massInElephantUnits: Int, expectedFuel: Int) ->
        dynamicTest("Fuel for $massInElephantUnits is $expectedFuel") {
            assertEquals(expectedFuel, fuelForModuleMass(massInElephantUnits, withFuelMass = true))
        }
    }
}
