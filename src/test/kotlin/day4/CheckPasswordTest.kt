package day4

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class CheckPasswordTest {
    private val examples = listOf(
        "112233" to true,
        "123444" to false,
        "111122" to true
    )

    @TestFactory
    fun testCheckPassword() = examples.map { (password: String, expectedOutput: Boolean) ->
        dynamicTest("Output for checking password $password is $expectedOutput") {
            assertEquals(expectedOutput, checkPassword(password))
        }
    }
}