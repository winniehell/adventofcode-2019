package day10

import getInput
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.locationtech.jts.algorithm.Angle
import org.locationtech.jts.geom.Coordinate
import kotlin.math.PI

internal class VaporizeTest {
    private val examples = listOf(
        Triple(
            """
                .#..#
                .....
                #####
                ....#
                ...##
            """.trimIndent(),
            Coordinate(3.0, 4.0),
            """
                .#...
                .....
                .....
                .....
                ...#.
            """.trimIndent()
        ),

        Triple(
            """
                .#....#####...#..
                ##...##.#####..##
                ##...#...#.#####.
                ..#.....#...###..
                ..#.#.....#....##
            """.trimIndent(),
            Coordinate(8.0, 3.0),
            """
                ........#.....#..
                ..........#.....#
                .................
                ........#....##..
                .................
            """.trimIndent()
        ),

        Triple(
            """
                ........#.....#..
                ..........#.....#
                .................
                ........#....##..
                .................
            """.trimIndent(),
            Coordinate(8.0, 3.0),
            """
                .................
                .................
                .................
                ........#.....#..
                .................
            """.trimIndent()
        )
    )

    @TestFactory
    fun testVaporize() = examples.map { (before: String, position: Coordinate, after: String) ->
        dynamicTest("Vaporizes asteroids") {
            val asteroidMap = AsteroidMap(before)
            val station = asteroidMap.asteroids.find { it.coordinate == position }!!

            asteroidMap.vaporizeAsteroids(station.visibleAsteroids)

            assertEquals(after, asteroidMap.toString())
        }
    }

    @Test
    fun partTwo() {
        val mapData = getInput(day = 10).readText()
        val asteroidMap = AsteroidMap(mapData)
        val station = asteroidMap.findBestStation()
        val laserStart = Coordinate(station.coordinate.x, -1.0)

        val vaporizedAsteroids = mutableListOf<Asteroid>()
        while (asteroidMap.asteroids.size > 1) {
            vaporizedAsteroids.addAll(
                station.visibleAsteroids.sortedBy {
                    val angle = Angle.angleBetween(laserStart, station.coordinate, it.coordinate)
                    if (it.coordinate.x < station.coordinate.x)
                        2 * PI - angle
                    else
                        angle
                }
            )
            asteroidMap.vaporizeAsteroids(station.visibleAsteroids)
        }

        assertEquals(Coordinate(2.0, 4.0), vaporizedAsteroids[199].coordinate)
    }
}