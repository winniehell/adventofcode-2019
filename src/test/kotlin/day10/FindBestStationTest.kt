package day10

import getInput
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory
import org.locationtech.jts.geom.Coordinate

internal class FindBestStationTest {
    private val examples = mapOf(
        """
            .#..#
            .....
            #####
            ....#
            ...##
        """.trimIndent() to Pair(Coordinate(3.0, 4.0), 8),

        """
            ......#.#.
            #..#.#....
            ..#######.
            .#.#.###..
            .#..#.....
            ..#....#.#
            #..#....#.
            .##.#..###
            ##...#..#.
            .#....####
        """.trimIndent() to Pair(Coordinate(5.0, 8.0), 33),

        """
            #.#...#.#.
            .###....#.
            .#....#...
            ##.#.#.#.#
            ....#.#.#.
            .##..###.#
            ..#...##..
            ..##....##
            ......#...
            .####.###.
        """.trimIndent() to Pair(Coordinate(1.0, 2.0), 35),

        """
            .#..#..###
            ####.###.#
            ....###.#.
            ..###.##.#
            ##.##.#.#.
            ....###..#
            ..#.#..#.#
            #..#.#.###
            .##...##.#
            .....#.#..
        """.trimIndent() to Pair(Coordinate(6.0, 3.0), 41),

        """
            .#..##.###...#######
            ##.############..##.
            .#.######.########.#
            .###.#######.####.#.
            #####.##.#.##.###.##
            ..#####..#.#########
            ####################
            #.####....###.#.#.##
            ##.#################
            #####.##.###..####..
            ..######..##.#######
            ####.##.####...##..#
            .#####..#.######.###
            ##...#.##########...
            #.##########.#######
            .####.#.###.###.#.##
            ....##.##.###..#####
            .#.#.###########.###
            #.#.#.#####.####.###
            ###.##.####.##.#..##
        """.trimIndent() to Pair(Coordinate(11.0, 13.0), 210),

        getInput(day = 10).readText() to Pair(Coordinate(17.0, 23.0), 296)
    )

    @TestFactory
    fun testFindBestStation() = examples.map { (mapData: String, expectedResult: Pair<Coordinate, Int>) ->
        dynamicTest("Finds the best place") {
            val asteroidMap = AsteroidMap(mapData)

            val bestStation = asteroidMap.findBestStation()
            assertEquals(expectedResult.first, bestStation.coordinate)
            assertEquals(expectedResult.second, bestStation.visibleAsteroids.size)
        }

    }
}
