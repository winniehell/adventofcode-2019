package day10

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class ToStringTest {
    private val examples = listOf(
        """
            .#..#
            .....
            #####
            ....#
            ...##
        """.trimIndent()
    )

    @TestFactory
    fun testToString() = examples.map { mapData: String ->
        dynamicTest("Outputs the input") {
            val asteroidMap = AsteroidMap(mapData)
            assertEquals(mapData, asteroidMap.toString())
        }
    }
}