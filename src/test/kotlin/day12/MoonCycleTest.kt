package day12

import asSequence
import getInput
import leastCommonMultiple
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class MoonCycleTest {
    private val examples = listOf(
        """
            <x=-1, y=0, z=2>
            <x=2, y=-10, z=-7>
            <x=4, y=-8, z=8>
            <x=3, y=5, z=-1>
        """.trimIndent() to 2772L,

        """
            <x=-8, y=-10, z=0>
            <x=5, y=5, z=10>
            <x=2, y=-7, z=3>
            <x=9, y=-8, z=-3>
        """.trimIndent() to 4686774924L,

        getInput(day = 12).readText().trim() to 1733885776948501552L // 1638181428413520L // too big
    )

    @TestFactory
    fun testCycle() = examples.map { (input: String, expectedCycleLength: Long) ->
        dynamicTest("Finds moon cycle") {
            val moons =
                input
                    .split("\n")
                    .map { Moon.fromString(it) }

            val coordinates = moons
                .flatMap { it.position.asSequence().asIterable() }
                .chunked(1)
                .map { it.toMutableList() }

            val cycles = coordinates.map { null as Long? }.toMutableList()

            for (doubleCycleLength in (2..Int.MAX_VALUE)) {
                moons.forEach { it.updateVelocity(moons.minus(it)) }
                moons.forEach { it.updatePosition() }

                moons
                    .flatMap { it.position.asSequence().asIterable() }
                    .forEachIndexed { i, value -> coordinates[i].add(value) }

                if (doubleCycleLength % 2 == 1) continue

                val cycleLength = doubleCycleLength / 2
                cycles.forEachIndexed { i, value ->
                    if (value != null) return@forEachIndexed
                    if (coordinates[i].subList(0, cycleLength) ==
                        coordinates[i].subList(cycleLength, doubleCycleLength)
                    ) {
                        cycles[i] = cycleLength.toLong()
                    }
                }

                if (!cycles.contains(null)) break
            }

            val cycleSteps = cycles.filterNotNull().reduce(::leastCommonMultiple)
            assertEquals(expectedCycleLength, cycleSteps)
        }
    }
}