package day12

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class ParsingMoonTest {
    private val examples = listOf(
        """
            <x=-1, y=0, z=2>
            <x=2, y=-10, z=-7>
            <x=4, y=-8, z=8>
            <x=3, y=5, z=-1>
        """.trimIndent() to
                """
                    pos=<x=-1, y=  0, z= 2>, vel=<x= 0, y= 0, z= 0>
                    pos=<x= 2, y=-10, z=-7>, vel=<x= 0, y= 0, z= 0>
                    pos=<x= 4, y= -8, z= 8>, vel=<x= 0, y= 0, z= 0>
                    pos=<x= 3, y=  5, z=-1>, vel=<x= 0, y= 0, z= 0>
                """.trimIndent(),

        """
            <x=-8, y=-10, z=0>
            <x=5, y=5, z=10>
            <x=2, y=-7, z=3>
            <x=9, y=-8, z=-3>
        """.trimIndent() to
                """
                    pos=<x= -8, y=-10, z=  0>, vel=<x=  0, y=  0, z=  0>
                    pos=<x=  5, y=  5, z= 10>, vel=<x=  0, y=  0, z=  0>
                    pos=<x=  2, y= -7, z=  3>, vel=<x=  0, y=  0, z=  0>
                    pos=<x=  9, y= -8, z= -3>, vel=<x=  0, y=  0, z=  0>
                """.trimIndent()
    )

    @TestFactory
    fun testParsing() = examples.map { (input: String, expectedOutput: String) ->
        dynamicTest("Parses and prints moon correctly") {
            assertEquals(
                expectedOutput
                    .split("\n")
                    .map { Moon.fromString(it) }
                    .map { it.toString() },
                input
                    .split("\n")
                    .map { Moon.fromString(it) }
                    .map { it.toString() }
            )
        }
    }
}