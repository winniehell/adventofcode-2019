package day12

import getInput
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class MoonEnergyTest {
    private val examples = listOf(
        Triple(
            """
                <x=-1, y=0, z=2>
                <x=2, y=-10, z=-7>
                <x=4, y=-8, z=8>
                <x=3, y=5, z=-1>
            """.trimIndent(), 10, 179
        ),

        Triple(
            """
                <x=-8, y=-10, z=0>
                <x=5, y=5, z=10>
                <x=2, y=-7, z=3>
                <x=9, y=-8, z=-3>
            """.trimIndent(), 100, 1940
        ),

        Triple(
            getInput(day = 12).readText().trim(),
            1000,
            6423
        )
    )

    @TestFactory
    fun testEnergy() = examples.map { (input: String, numSteps: Int, expectedEnergy: Int) ->
        dynamicTest("Calculates moon energy correctly") {
            val moons =
                input
                    .split("\n")
                    .map { Moon.fromString(it) }

            for(i in 1..numSteps) {
                moons.forEach { it.updateVelocity(moons.minus(it)) }
                moons.forEach { it.updatePosition() }
            }

            assertEquals(
                expectedEnergy,
                moons.sumBy { it.energy.toInt() }
            )
        }
    }
}