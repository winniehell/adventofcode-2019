package day2

import getInput
import intcode.Program
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

internal class ProgramTest {
    private val examples = listOf(
        "1,0,0,0,99" to listOf(2, 0, 0, 0, 99),
        "2,3,0,3,99" to listOf(2, 3, 0, 6, 99),
        "2,4,4,5,99,0" to listOf(2, 4, 4, 5, 99, 9801),
        "1,1,1,4,99,5,6,0,99" to listOf(30, 1, 1, 4, 2, 5, 6, 0, 99)
    )

    @TestFactory
    fun testProgram() = examples.map { (code: String, expectedMemory: List<Int>) ->
        dynamicTest("Resulting memory for program $code is $expectedMemory") {
            val program = Program(code)

            runBlocking {
                program.run()
                assertEquals(expectedMemory, program.memory)
            }
        }
    }

    @Test
    fun partOne() {
        val code = getInput(day = 2).readText()
        val program = Program(code)
        program.setNounAndVerb(12, 2)
        runBlocking {
            program.run()
            val output = program.memory[0]
            assertEquals(4930687, output)
        }
    }

    @Test
    fun partTwo() {
        val code = getInput(day = 2).readText()
        val program = Program(code)
        program.setNounAndVerb(53, 35)
        runBlocking {
            program.run()
            val output = program.memory[0]
            assertEquals(19690720, output)
        }
    }
}
