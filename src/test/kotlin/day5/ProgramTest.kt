package day5

import getInput
import intcode.Program
import kotlinx.coroutines.channels.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

internal class ProgramTest {
    @Test
    fun testInputOutput() {
        val input = 42
        val code = "3,0,4,0,99"
        val program = Program(code)

        runBlocking {
            launch {
                program.run()
            }

            program.input.send(42)
            assertEquals(input, program.output.receive())
        }
    }

    @Test
    fun testParameterModes() {
        val code = "1002,4,3,4,33"
        val program = Program(code)

        runBlocking {
            program.run()
            assertEquals(listOf(1002, 4, 3, 4, 99), program.memory)
        }
    }

    @Test
    fun partOne() {
        val code = getInput(day = 5).readText()
        val program = Program(code)
        runBlocking {
            launch {
                program.run()
            }

            program.input.send(1)
            assertEquals(12234644, program.output.toList().last())
        }
    }

    @TestFactory
    fun testEqual() = listOf(
        "3,9,8,9,10,9,4,9,99,-1,8",
        "3,3,1108,-1,8,3,4,3,99"
    ).flatMap { code ->
        listOf(
            1 to 0,
            8 to 1,
            10 to 0
        ).map { (input, output) ->
            dynamicTest("Program $code outputs $output for $input") {
                val program = Program(code)
                runBlocking {
                    launch {
                        program.run()
                    }

                    program.input.send(input)
                    assertEquals(output, program.output.receive())
                }
            }
        }
    }

    @TestFactory
    fun testLessThan() = listOf(
        "3,9,7,9,10,9,4,9,99,-1,8",
        "3,3,1107,-1,8,3,4,3,99"
    ).flatMap { code ->
        listOf(
            3 to 1,
            8 to 0,
            10 to 0
        ).map { (input, output) ->
            dynamicTest("Program $code outputs $output for $input") {
                val program = Program(code)
                runBlocking {
                    launch {
                        program.run()
                    }

                    program.input.send(input)
                    assertEquals(output, program.output.receive())
                }
            }
        }
    }

    @TestFactory
    fun testJump() = listOf(
        "3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9",
        "3,3,1105,-1,9,1101,0,0,12,4,12,99,1"
    ).flatMap { code ->
        mapOf(
            0 to 0,
            -1 to 1,
            10 to 1
        ).map { (input, output) ->
            dynamicTest("Program $code outputs $output for $input") {
                val program = Program(code)
                runBlocking {
                    launch {
                        program.run()
                    }

                    program.input.send(input)
                    assertEquals(output, program.output.receive())
                }

            }
        }
    }

    @TestFactory
    fun testExample() = mapOf(
        -3 to 999,
        8 to 1000,
        42 to 1001
    ).map { (input, output) ->
        val code = """
            3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
            1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
            999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
        """.replace("\\s".toRegex(), "")
        dynamicTest("Program $code outputs $output for $input") {
            val program = Program(code)
            runBlocking {
                launch {
                    program.run()
                }

                program.input.send(input)
                assertEquals(output, program.output.receive())
            }

        }
    }

    @Test
    fun partTwo() {
        val code = getInput(day = 5).readText()
        val program = Program(code)

        runBlocking {
            launch {
                program.run()
            }

            program.input.send(5)
            assertEquals(3508186, program.output.receive())
        }
    }
}
