package day8

import getInput
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ImageTest {
    @Test
    fun testCountPixels() {
        val image = Image(data = "123456789012", width = 3, height = 2)
        assertEquals(1, image.layers[0].countPixels(1))
        assertEquals(0, image.layers[0].countPixels(99))
        assertEquals(1, image.layers[1].countPixels(1))
        assertEquals(0, image.layers[1].countPixels(99))
    }

    @Test
    fun partOne() {
        val data = getInput(day = 8).readText().trim()
        val image = Image(data = data, width = 25, height = 6)
        val checksumLayer = image.layers.minBy { it.countPixels(0) }!!
        val checksum = checksumLayer.countPixels(1) * checksumLayer.countPixels(2)
        assertEquals(2760, checksum)
    }

    @Test
    fun testMergeLayers() {
        val data = "0222112222120000"
        val image = Image(data = data, width = 2, height = 2)
        val merged = image.mergeLayers()
        val expectedResult = listOf(
            listOf(0, 1),
            listOf(1, 0)
        )
        assertEquals(expectedResult, merged.rows)
    }

    @Test
    fun partTwo() {
        val data = getInput(day = 8).readText().trim()
        val image = Image(data = data, width = 25, height = 6)
        val merged = image.mergeLayers()
        assertEquals("""
             ██   ██  █  █ ████ ███  
            █  █ █  █ █  █ █    █  █ 
            █  █ █    █  █ ███  ███  
            ████ █ ██ █  █ █    █  █ 
            █  █ █  █ █  █ █    █  █ 
            █  █  ███  ██  ████ ███  
        """.trimIndent(), merged.drawToString())
    }
}
