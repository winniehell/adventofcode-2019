package day7

import getInput
import intcode.Program
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

internal class AmplifierTest {
    val examples = listOf(
        Triple(
            "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0",
            listOf(4, 3, 2, 1, 0),
            43210
        ),
        Triple(
            "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0",
            listOf(0, 1, 2, 3, 4),
            54321
        ),
        Triple(
            "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0",
            listOf(1, 0, 4, 3, 2),
            65210
        )
    )

    @TestFactory
    fun testAmplifier() = examples.map { (code: String, phaseSettings: List<Int>, expectedThrusterSignal: Int) ->
        dynamicTest("Thruster signal for $code and $phaseSettings is $expectedThrusterSignal") {
            var signal = 0
            for (setting in phaseSettings) {
                val program = Program(code)
                runBlocking {
                    launch {
                        program.run()
                    }

                    program.input.send(setting)
                    program.input.send(signal)
                    signal = program.output.receive()
                }
            }

            assertEquals(expectedThrusterSignal, signal)
        }
    }

    @Test
    fun partOne() {
        val code = getInput(day = 7).readText()
        val phaseSettings = listOf(2, 4, 1, 0, 3)
        var signal = 0
        for (setting in phaseSettings) {
            val program = Program(code)
            runBlocking {
                launch {
                    program.run()
                }

                program.input.send(setting)
                program.input.send(signal)
                signal = program.output.receive()
            }
        }
        assertEquals(17406, signal)
    }
}
