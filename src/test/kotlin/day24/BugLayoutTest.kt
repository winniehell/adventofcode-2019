package day24

import getInput
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory

internal class BugLayoutTest {
    private val exampleInput = """
        ....#
        #..#.
        #..##
        ..#..
        #....
    """.trimIndent()

    private val expectedNeighbors = mapOf(
        Pair(0,0) to 1,
        Pair(0, 1) to 1,
        Pair(2, 0) to 0,
        Pair(3, 2) to 2
    )


    @TestFactory
    fun testNeighbors() = expectedNeighbors.map { (position: Pair<Int, Int>, numNeighbors: Int) ->
        val bugLayout = BugLayout(exampleInput)

        dynamicTest("$position has $numNeighbors neighbors") {
            val (x, y) = position
            assertEquals(numNeighbors, bugLayout.countBugNeighbors(x, y))
        }
    }

    private val expectedSteps = listOf(
        exampleInput,

        """
            #..#.
            ####.
            ###.#
            ##.##
            .##..
        """.trimIndent(),

        """
            #####
            ....#
            ....#
            ...#.
            #.###
        """.trimIndent(),

        """
            #....
            ####.
            ...##
            #.##.
            .##.#
        """.trimIndent(),

        """
            ####.
            ....#
            ##..#
            .....
            ##...
        """.trimIndent()
    )

    @TestFactory
    fun testSteps() = expectedSteps.mapIndexed { stepIndex : Int, expectedStep : String ->
        val bugLayout = BugLayout(exampleInput)

        dynamicTest("Step $stepIndex is correct") {
            assertEquals(expectedStep, bugLayout.calculateStep(stepIndex).toString())
        }
    }

    @Test
    fun testFindCycle() {
        val expectedCycleState = """
            .....
            .....
            .....
            #....
            .#...
        """.trimIndent()
        val bugLayout = BugLayout(exampleInput)
        assertEquals(expectedCycleState, bugLayout.findCycle().toString())
    }

    private val biodiversity = mapOf(
        BugLayout("""
            .....
            .....
            .....
            #....
            .#...
        """.trimIndent()) to 2129920,

        BugLayout(getInput(day = 24).readText()).findCycle() to 18371095
    )

    @TestFactory
    fun testBiodiversity() = biodiversity.map { (bugLayout : BugLayout, expectedBiodiversity: Int) ->
        dynamicTest("biodiversity is $expectedBiodiversity") {
            assertEquals(expectedBiodiversity, bugLayout.biodiversity)
        }
    }
}