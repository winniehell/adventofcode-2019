package day6

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class TotalOrbitCountTest {
    private val examples =
        mapOf(
            listOf(
                "COM)B",
                "B)C",
                "C)D",
                "D)E",
                "E)F",
                "B)G",
                "G)H",
                "D)I",
                "E)J",
                "J)K",
                "K)L"
            ) to 42
        )

    @TestFactory
    fun testOrbitCount() = examples.map { (edges: List<String>, expectedCount: Int) ->
        dynamicTest("Output for counting orbits of $edges is $expectedCount") {
            assertEquals(expectedCount, OrbitMap(edges).countOrbits())
        }
    }
}