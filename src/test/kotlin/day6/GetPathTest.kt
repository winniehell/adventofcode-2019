package day6

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class GetPathTest {
    val map = OrbitMap(
        listOf(
            "COM)B",
            "B)C",
            "C)D",
            "D)E",
            "E)F",
            "B)G",
            "G)H",
            "D)I",
            "E)J",
            "J)K",
            "K)L"
        )
    )

    private val examples = mapOf(
        "COM" to OrbitPath(listOf("COM")),
        "C" to OrbitPath(listOf("COM", "B", "C"))
    )

    @TestFactory
    fun testGetPath() = examples.map { (name: String, expectedPath: OrbitPath) ->
        dynamicTest("Path for $name is $expectedPath") {
            assertEquals(expectedPath, map.getPathFromCenterOfMass(name))
        }
    }
}