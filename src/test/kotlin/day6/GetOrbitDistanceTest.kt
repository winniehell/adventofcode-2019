package day6

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

class GetOrbitDistanceTest {
    val map = OrbitMap(
        listOf(
            "COM)B",
            "B)C",
            "C)D",
            "D)E",
            "E)F",
            "B)G",
            "G)H",
            "D)I",
            "E)J",
            "J)K",
            "K)L"
        )
    )

    private val examples = mapOf(
        ("K" to "I") to 4
    )

    @TestFactory
    fun testGetOrbitDistance() = examples.map { (objectPair: Pair<String, String>, expectedDistance: Int) ->
        val (from, to) = objectPair
        dynamicTest("Path from $from to $to has length $expectedDistance") {
            assertEquals(expectedDistance, map.getOrbitDistance(from, to))
        }
    }
}