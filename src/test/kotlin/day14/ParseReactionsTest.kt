package day14

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class ParseReactionsTest {
    private val examples = listOf(
        """
            10 ORE => 10 A
            1 ORE => 1 B
            7 A, 1 B => 1 C
            7 A, 1 C => 1 D
            7 A, 1 D => 1 E
            7 A, 1 E => 1 FUEL
        """.trimIndent() to listOf(
            Reaction(
                input = sortedMapOf("ORE" to 10L),
                output = sortedMapOf("A" to 10L)
            ),
            Reaction(
                input = sortedMapOf("ORE" to 1L),
                output = sortedMapOf("B" to 1L)
            ),
            Reaction(
                input = sortedMapOf("A" to 7L, "B" to 1L),
                output = sortedMapOf("C" to 1L)
            ),
            Reaction(
                input = sortedMapOf("A" to 7L, "C" to 1L),
                output = sortedMapOf("D" to 1L)
            ),
            Reaction(
                input = sortedMapOf("A" to 7L, "D" to 1L),
                output = sortedMapOf("E" to 1L)
            ),
            Reaction(
                input = sortedMapOf("A" to 7L, "E" to 1L),
                output = sortedMapOf("FUEL" to 1L)
            )
        )
    )

    @TestFactory
    fun testParsing() = examples.map { (input: String, expectedReactions: List<Reaction>) ->
        dynamicTest("Parses the reactions") {
            val factory = Nanofactory(input.split("\n"))
            assertEquals(expectedReactions, factory.reactions)
        }
    }
}
