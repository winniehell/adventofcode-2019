package day14

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.TestFactory

internal class SubstituteReactionTest {
    private val examples = listOf(
        Triple(
            Reaction.fromString("7 A, 1 B => 1 C"),
            Reaction.fromString("10 ORE => 10 A"),
            Reaction.fromString("1 B, 10 ORE => 1 C, 3 A")
        ),

        Triple(
            Reaction.fromString("3 A, 4 B => 1 AB"),
            Reaction.fromString("8 ORE => 3 B"),
            Reaction.fromString("3 A, 16 ORE => 1 AB, 2 B")
        )
    )

    @TestFactory
    fun testParsing() = examples.map { (first: Reaction, second: Reaction, expectedResult: Reaction) ->
        dynamicTest("Substituting $first with $second results in $expectedResult") {
            assertEquals(expectedResult, first.substitute(second))
        }
    }
}
