package day24

const val bugCharacter = '#'
const val emptySpaceCharacter = '.'

class BugLayout private constructor(private val lines: List<String>) {
    constructor(input: String) : this(input.trim().split("\n"))

    private fun hasBug(x: Int, y: Int) =
        if (x < 0 || y < 0)
            false
        else if (y > lines.lastIndex)
            false
        else if (x > lines[y].lastIndex)
            false
        else
            lines[y][x] == bugCharacter

    fun countBugNeighbors(x: Int, y: Int) =
        sequenceOf(
            x - 1 to y,
            x to y - 1,
            x to y + 1,
            x + 1 to y
        ).sumBy { (nx, ny) ->
            if (hasBug(nx, ny))
                1
            else
                0
        }

    private fun calculateNextState(x: Int, y: Int) =
        when (countBugNeighbors(x, y)) {
            1 -> bugCharacter
            2 -> {
                if (hasBug(x, y))
                    emptySpaceCharacter
                else
                    bugCharacter
            }
            else ->
                emptySpaceCharacter
        }

    private fun calculateNextLine(y: Int, line: String) =
        line.indices
            .map { x -> calculateNextState(x, y) }
            .joinToString("")

    private fun calculateNextStep() = BugLayout(lines.mapIndexed(::calculateNextLine))

    fun calculateStep(stepIndex: Int): BugLayout = when {
        stepIndex == 0 -> this
        stepIndex > 0 -> calculateNextStep().calculateStep(stepIndex - 1)
        else -> throw IllegalArgumentException("Step $stepIndex must be at least 0!")
    }

    override fun toString() = lines.joinToString("\n")

    override fun equals(other: Any?) = other is BugLayout && other.toString() == toString()

    fun findCycle(): BugLayout {
        val previousSteps = mutableSetOf(toString())
        var nextStep = calculateNextStep()
        while (!previousSteps.contains(nextStep.toString())) {
            previousSteps.add(nextStep.toString())
            nextStep = nextStep.calculateNextStep()
        }
        return nextStep
    }

    val biodiversity: Int
        get() {
            var result = 0
            var power = 1
            for (line in lines) {
                for (cell in line) {
                    if (cell == bugCharacter) {
                        result += power
                    }

                    power *= 2
                }
            }
            return result
        }
}
