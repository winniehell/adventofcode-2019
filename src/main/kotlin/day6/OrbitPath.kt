package day6

import kotlin.math.min

data class OrbitPath(val objectNames: List<String>) {
    val length = objectNames.size

    fun lastCommonIndex(other: OrbitPath) =
        (0 until min(length, other.length)).last { i -> objectNames[i] == other.objectNames[i] }
}
