package day6

class OrbitMap(encodedEdges: List<String>) {
    private val objectMap = mutableMapOf(CenterOfMass.name to CenterOfMass)

    init {
        fun getOrPutObject(name: String): OrbitingObject =
            objectMap.getOrPut(name, { OrbitingObject(name) })

        encodedEdges.forEach { edgeString ->
            val (centerName, name) = edgeString.split(')')
            val center = getOrPutObject(centerName)
            getOrPutObject(name).orbitsAround = center
        }
    }

    fun countOrbits() = objectMap.values.sumBy { it.countOrbits() }

    fun getPathFromCenterOfMass(name: String): OrbitPath {
        val objectNames = mutableListOf<String>()
        var currentObject = objectMap[name]
        while (currentObject != null) {
            objectNames.add(0, currentObject.name)
            currentObject = currentObject.orbitsAround
        }
        return OrbitPath(objectNames)
    }

    fun getOrbitDistance(from: String, to: String): Int {
        val fromPath = getPathFromCenterOfMass(from)
        val toPath = getPathFromCenterOfMass(to)
        val lastCommonIndex = fromPath.lastCommonIndex(toPath)
        return (fromPath.length - lastCommonIndex - 1) + (toPath.length - lastCommonIndex - 1)
    }
}
