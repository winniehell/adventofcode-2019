package day6

import getInput

fun main() {
    val map = OrbitMap(getInput(day = 6).readLines())
    println("number of total orbits: ${map.countOrbits()}")

    println("distance from you to Santa: ${map.getOrbitDistance("YOU", "SAN") - 2}")
}
