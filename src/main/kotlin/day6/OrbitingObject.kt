package day6

open class OrbitingObject(val name: String) {
    var orbitsAround: OrbitingObject? = null

    open fun countOrbits(): Int = orbitsAround!!.countOrbits() + 1
}

val CenterOfMass = object : OrbitingObject("COM") {
    override fun countOrbits() = 0
}
