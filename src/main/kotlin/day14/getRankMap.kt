package day14

import java.util.*

fun getRankMap(edges: Map<String, Set<String>>, startName: String, rootName: String): Map<String, Int> {
    val rankMap = mutableMapOf(rootName to 0)

    val resolutionStack = Stack<String>()
    resolutionStack.push(startName)
    while (!resolutionStack.empty()) {
        val missingNeighbors = edges.getValue(resolutionStack.peek()).filterNot { rankMap.containsKey(it) }
        if (missingNeighbors.isEmpty()) {
            val name = resolutionStack.pop()
            val neighborRanks = edges.getValue(name).map { rankMap[it]!! }
            rankMap[name] = neighborRanks.max()!! + 1
            continue
        }

        missingNeighbors.forEach { resolutionStack.push(it) }
    }

    return rankMap
}
