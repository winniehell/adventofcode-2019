package day14

import java.util.*
import kotlin.math.sign

private const val inputOutputDelimiter = " => "
private const val chemicalDelimiter = ", "

private fun parseChemical(value: String): Pair<String, Long> {
    val (amount, name) = value.split(" ")
    return name to amount.toLong()
}

private fun ceilDiv(a: Long, b: Long) = a / b + (a % b).sign

class Reaction(val input: SortedMap<String, Long>, val output: SortedMap<String, Long>) {
    override fun toString() = sequenceOf(input.entries, output.entries)
        .map { it.joinToString { (name, amount) -> "$amount $name" } }
        .joinToString(inputOutputDelimiter)

    override fun equals(other: Any?): Boolean {
        return toString().equals(other.toString())
    }

    override fun hashCode() = Objects.hash(input, output)

    fun scale(multiplier: Long) =
        Reaction(
            input.mapValues { (_, value) -> multiplier * value }.toSortedMap(),
            output.mapValues { (_, value) -> multiplier * value }.toSortedMap()
        )

    fun substitute(other: Reaction): Reaction {
        val commonChemicals = input.keys.intersect(other.output.keys)
        val multiplier = commonChemicals
            .map { ceilDiv(input.getValue(it), other.output.getValue(it)) }
            .max()!!

        if (multiplier > 1) {
            return substitute(other.scale(multiplier))
        }

        val mergedInput = input.minus(commonChemicals)
            .mapValues { (name, amount) -> amount + other.input.getOrDefault(name, 0) }
            .plus(other.input.minus(input.keys))
            .toSortedMap()

        val additionalOutput = other.output
            .mapValues { (name, amount) ->
                if (commonChemicals.contains(name))
                    amount - input[name]!!
                else
                    amount
            }
            .filterValues { it > 0 }

        val mergedOutput = output
            .mapValues { (name, amount) -> amount + additionalOutput.getOrDefault(name, 0) }
            .plus(additionalOutput.minus(output.keys))
            .toSortedMap()

        return Reaction(mergedInput, mergedOutput)
    }

    fun hasOutput(name: String): Boolean = output.containsKey(name)

    companion object {
        fun fromString(value: String): Reaction {
            val (input, output) = value.split(inputOutputDelimiter)
                .map { encodedChemicals ->
                    encodedChemicals
                        .split(chemicalDelimiter)
                        .map(::parseChemical)
                        .toMap()
                        .toSortedMap()
                }

            return Reaction(input, output)
        }
    }
}
