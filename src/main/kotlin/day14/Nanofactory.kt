package day14

class Nanofactory(encodedReactions: List<String>) {
    val reactions = encodedReactions.map { Reaction.fromString(it) }
    private val reactionEdges = reactions.map { it.output.firstKey() to it.input.keys }.toMap()
    private val rankMap = getRankMap(reactionEdges, "FUEL", "ORE")

    private fun reactionsByRank(rank: Int): List<Reaction> =
        reactions.filter { rankMap[it.output.firstKey()] == rank }

    fun getFuelPrice(amountOfFuel: Long = 1): Long {
        var fuelReaction = reactions.find { it.hasOutput("FUEL") }!!
        fuelReaction = fuelReaction.scale(amountOfFuel / fuelReaction.output.getValue("FUEL"))

        for (i in rankMap.values.max()!! - 1 downTo rankMap.values.min()!! + 1) {
            reactionsByRank(i).forEach {
                fuelReaction = fuelReaction.substitute(it)
            }
        }

        return fuelReaction.input.getValue("ORE")
    }

    fun getFuelPerCargo(amountOfOre: Long): Long {
        val price = getFuelPrice(1)
        if (price > amountOfOre) {
            return 0
        }

        val lowerBound = amountOfOre / price
        var fuelBounds = lowerBound to 2 * lowerBound

        while (fuelBounds.first < fuelBounds.second - 1) {
            val fuel = (fuelBounds.first + fuelBounds.second) / 2
            fuelBounds =
                if (getFuelPrice(fuel) > amountOfOre)
                    fuelBounds.first to fuel
                else
                    fuel to fuelBounds.second
        }

        return fuelBounds.first
    }
}
