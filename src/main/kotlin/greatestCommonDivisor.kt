fun greatestCommonDivisor(a: Long, b: Long): Long =
    if (b == 0L)
        a
    else
        greatestCommonDivisor(b, a % b)
