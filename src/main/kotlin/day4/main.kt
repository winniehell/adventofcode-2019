package day4

fun main() {
    var numValidPasswords = 0
    for (password in 156218..652527) {
        val isValid = checkPassword(password.toString())
        println("$password: $isValid")
        if (isValid) numValidPasswords += 1
    }
    println("total: $numValidPasswords")
}
