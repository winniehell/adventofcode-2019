package day4

fun containsDuplicateDigit(password: String): Boolean {
    for (i in 0 until password.lastIndex) {
        if (password[i] != password[i + 1]) continue
        if (i > 0 && password[i - 1] == password[i]) continue
        if (i < password.lastIndex - 1 && password[i + 1] == password[i + 2]) continue
        return true
    }
    return false
}

fun hasDecreasingDigits(password: String): Boolean {
    for (i in 0..password.length - 2) {
        if (password[i].toInt() > password[i + 1].toInt()) return true
    }
    return false
}

fun checkPassword(password: String): Boolean {
    if (password.length != 6) return false
    if (password.toInt().toString() != password) return false // TODO: breaks for leading zeros
    if (!containsDuplicateDigit(password)) return false
    if (hasDecreasingDigits(password)) return false
    return true
}
