package day8

const val transparent = 2

data class Layer(val rows: List<List<Int>>) {
    fun countPixels(value: Int) =
        rows.sumBy { row ->
            row.count { pixel -> pixel == value }
        }

    fun overlayAbove(other: Layer) = Layer(rows.mapIndexed { rowIndex, row ->
        row.mapIndexed { colIndex, value ->
            if (value == transparent) {
                other.rows
                    .getOrElse(rowIndex, { row })
                    .getOrElse(colIndex, { value })
            } else {
                value
            }
        }
    })

    fun drawToString() = rows
        .joinToString(separator = "\n") { row ->
            row.joinToString(separator = "") { value ->
                if (value == 0) " "
                else "█"
            }
        }
}
