package day8

data class Image(val data: String, val width: Int, val height: Int) {
    val layers = data
        .chunked(width)
        .chunked(height)
        .map { layerData ->
            layerData.map { rowData ->
                rowData.windowed(1).map { it.toInt() }
            }
        }
        .map { rows -> Layer(rows) }

    fun mergeLayers() = layers.reduce { a, b -> a.overlayAbove(b) }
}
