package day8

import getInput

fun main() {
    val data = getInput(day = 8).readText().trim()
    val image = Image(data = data, width = 25, height = 6)
    val checksumLayer = image.layers.minBy { it.countPixels(0) }!!
    val checksum = checksumLayer.countPixels(1) * checksumLayer.countPixels(2)
    println("checksum: $checksum")

    println("merged:")
    val merged = image.mergeLayers()
    println(merged.drawToString())
}
