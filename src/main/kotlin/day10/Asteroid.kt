package day10

import org.locationtech.jts.geom.Coordinate

data class Asteroid(val coordinate: Coordinate, val visibleAsteroids: MutableList<Asteroid> = mutableListOf())
