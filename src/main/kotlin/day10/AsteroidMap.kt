package day10

import org.locationtech.jts.algorithm.RobustLineIntersector
import org.locationtech.jts.geom.Coordinate

enum class MapSymbol(val char: Char) {
    Asteroid('#'),
    Emptiness('.');

    companion object {
        fun fromChar(value: Char) = values().find { it.char == value }!!
    }
}

private fun List<Asteroid>.forEachPair(action: (Pair<Asteroid, Asteroid>) -> Unit) =
    forEachIndexed { i, firstAsteroid ->
        listIterator(i + 1).asSequence()
            .forEach { secondAsteroid ->
                action.invoke(Pair(firstAsteroid, secondAsteroid))
            }
    }

private fun String.parseCoordinates(): List<Coordinate> =
    split('\n')
        .mapIndexed { y, row ->
            row
                .mapIndexed { x, value ->
                    when (MapSymbol.fromChar(value)) {
                        MapSymbol.Asteroid -> Coordinate(x.toDouble(), y.toDouble())
                        else -> null
                    }
                }
                .filterNotNull()
        }
        .flatten()


class AsteroidMap(mapData: String) {
    val asteroids = mapData.parseCoordinates().map { Asteroid(it) }.toMutableList()
    val xRange = (0 until mapData.indexOf('\n'))
    val yRange = (0..mapData.count { it == '\n' })

    init {
        updateVisibleAsteroids()
    }

    private fun updateVisibleAsteroids() {
        asteroids.forEach { asteroid -> asteroid.visibleAsteroids.clear() }

        val lineIntersector = RobustLineIntersector()
        asteroids.forEachPair { (firstAsteroid, secondAsteroid) ->
            val intersected = asteroids.any { a ->
                lineIntersector.computeIntersection(
                    a.coordinate,
                    firstAsteroid.coordinate,
                    secondAsteroid.coordinate
                )
                lineIntersector.isProper
            }

            if (!intersected) {
                firstAsteroid.visibleAsteroids.add(secondAsteroid)
                secondAsteroid.visibleAsteroids.add(firstAsteroid)
            }
        }
    }

    fun findBestStation(): Asteroid = asteroids.maxBy { asteroid -> asteroid.visibleAsteroids.size }!!

    fun vaporizeAsteroids(targets: List<Asteroid>) {
        asteroids.removeAll(targets)
        updateVisibleAsteroids()
    }

    override fun toString(): String {
        val coordinates = asteroids.map { it.coordinate }
        return yRange.joinToString("\n") { y ->
            xRange.joinToString("") { x ->
                if (coordinates.any { it == Coordinate(x.toDouble(), y.toDouble()) })
                    MapSymbol.Asteroid.char.toString()
                else
                    MapSymbol.Emptiness.char.toString()
            }
        }
    }
}
