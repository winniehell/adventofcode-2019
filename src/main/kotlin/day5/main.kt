package day5

import getInput
import intcode.Program
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    val code = getInput(day = 5).readText()
    for (input in listOf(1, 5)) {
        val program = Program(code)

        val programCoroutine = launch {
            program.run()
        }

        program.input.send(input)
        println("diagnostic code: ${program.output.receive()}")
        programCoroutine.cancel()
    }
}
