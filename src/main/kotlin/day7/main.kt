package day7

import getInput
import intcode.Program
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun combinations(numAmplifiers: Int, phaseSettingsRange: IntRange): List<List<Int>> {
    var result = phaseSettingsRange.map { listOf(it) }

    for (i in 1 until numAmplifiers) {
        result = result.flatMap { list ->
            phaseSettingsRange
                .filter { setting -> !list.contains(setting) }
                .map { setting -> list + setting }
        }
    }

    return result
}

fun main() = runBlocking {
    val code = getInput(day = 7).readText()
    val maxSignal = combinations(numAmplifiers = 5, phaseSettingsRange = 0..4)
        .map { phaseSettings ->
            var signal = 0
            for (setting in phaseSettings) {
                val program = Program(code)

                launch {
                    program.run()
                }

                program.input.send(setting)
                program.input.send(signal)
                signal = program.output.receive()
            }
            Pair(phaseSettings, signal)
        }
        .maxBy { (_, signal) -> signal }
    print("highest signal: $maxSignal")
}
