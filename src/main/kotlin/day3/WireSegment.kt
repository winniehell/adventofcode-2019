package day3

import org.locationtech.jts.geom.LineString

data class WireSegment(val lineString: LineString, val signalDelay: Double)
