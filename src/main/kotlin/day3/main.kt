package day3

import getInput

fun main() {
    val (firstWire, secondWire) = getInput(day = 3).readLines().map { Wire(it.trim()) }
    val intersections = firstWire.intersections(secondWire)
    val minDistance = intersections.map { (point, _, _) -> manhattanDistance(point) }.min()
    println("minimum distance to intersection: $minDistance")
}
