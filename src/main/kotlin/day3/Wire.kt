package day3

import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.geom.GeometryFactory
import org.locationtech.jts.math.Vector2D

val centralPort = Coordinate(0.0, 0.0)

fun parseDirection(c: Char): Vector2D {
    return when (c) {
        'D' -> Vector2D(0.0, -1.0)
        'L' -> Vector2D(-1.0, 0.0)
        'R' -> Vector2D(1.0, 0.0)
        'U' -> Vector2D(0.0, 1.0)
        else -> throw IllegalArgumentException("Unknown direction: $c")
    }
}

class Wire(input: String) {
    val segments = mutableListOf<WireSegment>()

    init {
        val factory = GeometryFactory()
        var currentPoint = Vector2D(centralPort)
        var signalDelay = 0.0

        val encodedSegments = input.split(',')
        encodedSegments.forEach { segmentString ->
            val direction = parseDirection(segmentString[0])
            val length = segmentString.substring(1).toInt()
            val nextPoint = currentPoint.add(direction.multiply(length.toDouble()))
            val coordinates = arrayOf(currentPoint.toCoordinate(), nextPoint.toCoordinate())
            segments.add(
                WireSegment(
                    factory.createLineString(coordinates),
                    signalDelay
                )
            )
            signalDelay += manhattanDistance(nextPoint.toCoordinate(), from = currentPoint.toCoordinate())
            currentPoint = nextPoint
        }
    }

    fun intersections(other: Wire) = segments.flatMap { firstSegment ->
        other.segments
            .map { secondSegment ->
                val intersection = firstSegment.lineString.intersection(secondSegment.lineString)
                if (intersection.isEmpty) null
                else if (intersection.coordinate == centralPort) null
                else WireIntersection(intersection.coordinate, firstSegment, secondSegment)
            }
            .filterNotNull()
    }

    fun findMinimumSignalDelayIntersection(other: Wire): Double {
        val intersections = intersections(other)
        return intersections
            .map { (point, firstSegment, secondSegment) ->
                var signalDelay = firstSegment.signalDelay + secondSegment.signalDelay
                signalDelay += manhattanDistance(point, from = firstSegment.lineString.startPoint.coordinate)
                signalDelay += manhattanDistance(point, from = secondSegment.lineString.startPoint.coordinate)
                signalDelay
            }
            .min() ?: 0.0
    }
}