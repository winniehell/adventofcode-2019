package day3

import org.locationtech.jts.geom.Coordinate
import org.locationtech.jts.math.Vector2D
import kotlin.math.absoluteValue

fun manhattanDistance(point: Coordinate, from: Coordinate = Coordinate(0.0, 0.0)): Double {
    val distance = Vector2D(point).subtract(Vector2D(from))
    return distance.x.absoluteValue + distance.y.absoluteValue
}
