package day3

import org.locationtech.jts.geom.Coordinate

data class WireIntersection(
    val point: Coordinate,
    val firstSegment: WireSegment,
    val secondSegment: WireSegment
)
