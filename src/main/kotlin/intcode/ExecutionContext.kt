package intcode

import kotlinx.coroutines.channels.Channel

class ExecutionContext(
    private val memory: MutableList<Int>,
    val input: Channel<Int>,
    val output: Channel<Int>
) {
    private var instructionPointer = memory.listIterator()

    fun readInstruction(readMode: ReadMode = ReadMode.Immediate): Int {
        val value = instructionPointer.next()

        if (readMode == ReadMode.Position) {
            if (debugMode) println(listOf("readInstruction", readMode, value, memory[value]))

            return memory[value]
        }

        if (debugMode) println(listOf("readInstruction", readMode, value))

        return value
    }

    fun writeInstruction(position: Int, value: Int) {
        memory[position] = value

        if (debugMode) println(listOf("writeInstruction", position, value))
    }

    fun hasInstruction() = instructionPointer.hasNext()

    fun jump(position: Int) {
        instructionPointer = memory.listIterator(position)
    }

    companion object {
        var debugMode: Boolean = false
    }
}
