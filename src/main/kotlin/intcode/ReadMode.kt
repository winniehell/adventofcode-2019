package intcode

enum class ReadMode(private val encodedValue: Int) {
    Position(0),
    Immediate(1);

    companion object {
        fun fromEncoded(encodedValue: Int) = values().find { it.encodedValue == encodedValue }!!
    }
}
