package intcode

fun dyadicOperation(getName: () -> String, f: (Int, Int) -> Int): suspend (ExecutionContext, Parameters) -> Unit =
    { context: ExecutionContext, parameters: Parameters ->
        val firstValue = parameters.next()
        val secondValue = parameters.next()
        val result = f(firstValue, secondValue)
        context.writeInstruction(context.readInstruction(), result)

        if (Operation.debugMode) println(listOf(getName(), firstValue, secondValue, result))
    }

enum class Operation(
    private val opcode: Int,
    val execute: suspend (context: ExecutionContext, parameters: Parameters) -> Unit
) {
    Addition(1, dyadicOperation({ Addition.name }) { a, b -> a + b }),
    Multiplication(2, dyadicOperation({ Multiplication.name }) { a, b -> a * b }),

    Input(3, { context, _ ->
        val position = context.readInstruction()
        val inputValue = context.input.receive()
        context.writeInstruction(position, inputValue)

        if (debugMode) println(listOf(Input.name, position, inputValue))
    }),

    Output(4, { context, parameters ->
        val outputValue = parameters.next()
        context.output.send(outputValue)

        if (debugMode) println(listOf(Output.name, outputValue))
    }),

    JumpIfTrue(5, { context, parameters ->
        val value = parameters.next()
        val position = parameters.next()
        if (value != 0) {
            context.jump(position)
        }

        if (debugMode) println(listOf(JumpIfTrue.name, value, position))
    }),

    JumpIfFalse(6, { context, parameters ->
        val value = parameters.next()
        val position = parameters.next()
        if (value == 0) {
            context.jump(position)
        }

        if (debugMode) println(listOf(JumpIfFalse.name, value, position))
    }),

    LessThan(7, dyadicOperation({ LessThan.name }) { a, b -> if (a < b) 1 else 0 }),
    Equal(8, dyadicOperation({ Equal.name }) { a, b -> if (a == b) 1 else 0 }),

    Halt(99, { _, _ -> });

    companion object {
        var debugMode = false
        fun fromOpcode(opcode: Int) = values().find { it.opcode == opcode }
    }
}
