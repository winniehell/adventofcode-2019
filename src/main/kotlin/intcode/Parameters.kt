package intcode

class Parameters(private val context: ExecutionContext, private var encodedParameterModes: Int) : Iterator<Int> {
    override fun next(): Int {
        val mode = ReadMode.fromEncoded(encodedParameterModes % 10)
        encodedParameterModes /= 10
        return context.readInstruction(mode)
    }

    override fun hasNext() = context.hasInstruction()
}
