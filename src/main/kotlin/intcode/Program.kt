package intcode

import kotlinx.coroutines.channels.Channel

const val programCellDelimiter = ","

class Program(code: String) {
    val memory = code.trim().split(programCellDelimiter).map { it.toInt() }.toMutableList()
    val input = Channel<Int>()
    val output = Channel<Int>()

    fun setNounAndVerb(noun: Int, verb: Int) {
        memory[1] = noun
        memory[2] = verb
    }

    suspend fun run() {
        val context = ExecutionContext(memory, input, output)
        while (context.hasInstruction()) {
            val nextValue = context.readInstruction()
            val opcode = nextValue % 100
            val operation =
                Operation.fromOpcode(opcode) ?: throw IllegalArgumentException("Unknown opcode: $opcode")
            if (operation == Operation.Halt) break
            val parameters = Parameters(context, nextValue / 100)
            operation.execute(context, parameters)
        }

        input.close()
        output.close()
    }
}
