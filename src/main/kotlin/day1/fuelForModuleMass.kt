package day1

import getInput

fun fuelForModuleMass(massInElephantUnits: Int, withFuelMass: Boolean = false) : Int {
    val fuel = Math.max(0, (massInElephantUnits / 3) - 2)

    if (withFuelMass && fuel > 0) {
        return fuel + fuelForModuleMass(fuel, withFuelMass)
    }

    return fuel
}

fun main() {
    val values = mutableListOf<Int>()
    getInput(1).useLines { it.forEach { line -> values += line.toInt() } }
    println("Number of values: ${values.count()}")
    println("Fuel without fuel mass: ${values.map {
        fuelForModuleMass(
            it,
            withFuelMass = false
        )
    }.sum()}")
    println("Fuel with fuel mass: ${values.map { fuelForModuleMass(it, withFuelMass = true) }.sum()}")
}
