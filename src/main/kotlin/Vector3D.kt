import org.locationtech.jts.math.Vector3D

fun Vector3D.add(other: Vector3D) = Vector3D(x + other.x, y + other.y, z + other.z)

fun Vector3D.asSequence() = sequenceOf(x, y, z)
