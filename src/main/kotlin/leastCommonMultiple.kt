fun leastCommonMultiple(a: Long, b: Long): Long =
    a * b / greatestCommonDivisor(a, b)
