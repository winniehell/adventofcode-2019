import java.io.File

fun getInput(day: Int) = File("input/day-$day.txt").inputStream().bufferedReader()
