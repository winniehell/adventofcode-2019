package day12

import add
import org.locationtech.jts.math.Vector3D
import kotlin.math.absoluteValue
import kotlin.math.sign

data class Moon(var position: Vector3D, var velocity: Vector3D = Vector3D(0.0, 0.0, 0.0)) {
    val potentialEnergy : Double
        get() = position.x.absoluteValue + position.y.absoluteValue + position.z.absoluteValue
    val kineticEnergy : Double
        get() = velocity.x.absoluteValue + velocity.y.absoluteValue + velocity.z.absoluteValue
    val energy : Double
        get() = potentialEnergy * kineticEnergy

    override fun toString(): String {
        val (x, y, z) = listOf(position.x, position.y, position.z).map { it.toInt() }
        val (vx, vy, vz) = listOf(velocity.x, velocity.y, velocity.z).map { it.toInt() }
        return "pos=<x=$x, y=$y, z=$z>, vel=<x=$vx, y=$vy, z=$vz>"
    }

    companion object {
        private val shortPattern = Regex("""<x= *(-?\d+), y= *(-?\d+), z= *(-?\d+)>""")
        private val longPattern = Regex("""pos=${shortPattern.pattern}, vel=${shortPattern.pattern}""")

        fun fromString(value: String): Moon {
            val parsedValues = sequenceOf(shortPattern, longPattern)
                .map { it.matchEntire(value) }
                .filterNotNull()
                .map { matchResult ->
                    matchResult.destructured
                        .toList()
                        .map { it.toDouble() }
                        .chunked(3)
                        .iterator()
                }
                .firstOrNull() ?: throw IllegalArgumentException("Invalid moon format: $value")

            val (x, y, z) = parsedValues.next()
            val position = Vector3D(x, y, z)

            val velocity =
                if (parsedValues.hasNext()) {
                    val (vx, vy, vz) = parsedValues.next()
                    Vector3D(vx, vy, vz)
                } else
                    Vector3D(0.0, 0.0, 0.0)

            return Moon(position, velocity)
        }
    }

    fun updatePosition() {
        position = position.add(velocity)
    }

    fun updateVelocity(otherMoons: List<Moon>) {
        val (vx, vy, vz) = (0..2)
            .map { i ->
                otherMoons.sumByDouble { other ->
                    (other.position.getComponent(i) - position.getComponent(i)).sign
                }
            }
        velocity = velocity.add(Vector3D(vx, vy, vz))
    }
}

private fun Vector3D.getComponent(index: Int) = when (index) {
    0 -> x
    1 -> y
    2 -> z
    else -> throw IllegalArgumentException("$index is not a valid component index!")
}
