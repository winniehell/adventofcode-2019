package day2

import getInput
import intcode.Program
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    val code = getInput(2).readText()

    for (noun in 0..99) {
        for (verb in 0..99) {
            val program = Program(code)
            program.setNounAndVerb(noun, verb)
            program.run()
            val output = program.memory[0]
            if (output == 19690720) {
                val result = 100 * noun + verb
                println("noun: $noun, verb: $verb, result: $result")
                return@runBlocking
            }
        }
    }
}
