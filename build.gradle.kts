plugins {
    java
    kotlin("jvm") version "1.3.61"
}

group = "de.winniehell"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.locationtech.jts", "jts-core", "1.16.1")
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.3.2")

    testImplementation("org.junit.jupiter", "junit-jupiter", "5.5.2")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}
